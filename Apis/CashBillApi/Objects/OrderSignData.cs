﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class OrderSignData {

    public OrderSignData() {
    }
    
    public OrderSignData(Order order, string secretPhrase) {
        Title = order.Title;
        Amount = order.Amount;
        Description = order.Description;
        AdditionalData = order.AdditionalData;
        ReturnUrl = order.ReturnUrl;
        NegativeReturnUrl = order.NegativeReturnUrl;
        PaymentChannel = order.PaymentChannel;
        LanguageCode = order.LanguageCode;
        PersonalData = order.PersonalData;
        Referer = order.Referer;
        SecretPhrase = secretPhrase;
    }

    public OrderSignData(string title, Amount amount, string description, string additionalData, string? returnUrl, string? negativeReturnUrl, string? paymentChannel, string? languageCode, PersonalData? personalData, string? referer, string secretPhrase) {
        Title = title;
        Amount = amount;
        Description = description;
        AdditionalData = additionalData;
        ReturnUrl = returnUrl;
        NegativeReturnUrl = negativeReturnUrl;
        PaymentChannel = paymentChannel;
        LanguageCode = languageCode;
        PersonalData = personalData;
        Referer = referer;
        SecretPhrase = secretPhrase;
    }

    public string Title { get; set; }
    public Amount Amount{ get; set; }
    public string Description{ get; set; }
    public string AdditionalData{ get; set; }
    public string? ReturnUrl{ get; set; }
    public string? NegativeReturnUrl{ get; set; }
    public string? PaymentChannel{ get; set; }
    public string? LanguageCode{ get; set; }
    public PersonalData? PersonalData{ get; set; }
    public string? Referer{ get; set; }
    public string SecretPhrase{ get; set; }
}