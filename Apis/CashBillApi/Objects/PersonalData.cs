﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class PersonalData {
    public PersonalData() {
    }

    public PersonalData(string? firstName, string? surname, string? email, string? country, string? city, string? postcode, string? street, string? house, string? flat) {
        FirstName = firstName;
        Surname = surname;
        Email = email;
        Country = country;
        City = city;
        Postcode = postcode;
        Street = street;
        House = house;
        Flat = flat;
    }

    public string? FirstName { get; set; }
    public string? Surname{ get; set; }
    public string? Email{ get; set; }
    public string? Country{ get; set; }
    public string? City{ get; set; }
    public string? Postcode{ get; set; }
    public string? Street{ get; set; }
    public string? House{ get; set; }
    public string? Flat{ get; set; }
}