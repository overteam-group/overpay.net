﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class PaymentChannel {
    public PaymentChannel() {
    }

    public PaymentChannel(string id, List<string> availableCurrencies, string name, string description, string logoUrl) {
        Id = id;
        AvailableCurrencies = availableCurrencies;
        Name = name;
        Description = description;
        LogoUrl = logoUrl;
    }

    public string Id { get; set; }
    public List<string> AvailableCurrencies{ get; set; }
    public string Name{ get; set; }
    public string Description{ get; set; }
    public string LogoUrl{ get; set; }
}