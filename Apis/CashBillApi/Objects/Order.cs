﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class Order {
    public Order() {
    }

    public Order(string title, Amount amount, string description, string additionalData, string? negativeReturnUrl, string? paymentChannel, string? languageCode, PersonalData? personalData, string? referer, string? sign) {
        Title = title;
        Amount = amount;
        Description = description;
        AdditionalData = additionalData;
        NegativeReturnUrl = negativeReturnUrl;
        PaymentChannel = paymentChannel;
        LanguageCode = languageCode;
        PersonalData = personalData;
        Referer = referer;
        Sign = sign;
    }

    public string Title { get; set; }
    public Amount Amount { get; set; }
    public string Description { get; set; }
    public string AdditionalData { get; set; }
    public string? ReturnUrl = Setup.CashBillOrderConfig.DefaultReturnUrl;
    public string? NegativeReturnUrl { get; set; }
    public string? PaymentChannel { get; set; }
    public string? LanguageCode { get; set; }
    public PersonalData? PersonalData { get; set; }
    public string? Referer { get; set; }
    public string Sign { get; set; }

}