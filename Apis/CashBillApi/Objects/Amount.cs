﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class Amount {
    public Amount() {
    }

    public Amount(string value, string currencyCode) {
        Value = value;
        CurrencyCode = currencyCode;
    }

    public string Value { get; set; }
    public string CurrencyCode { get; set; }
}