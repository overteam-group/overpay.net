﻿namespace OverPay.NET.Apis.CashBillApi.Objects; 

public class OrderInformation {
    public OrderInformation() {
    }
    
    public OrderInformation(string id, string title, string status, string paymentChannel, string description, string additionalData, Amount amount, Amount registeredAmount, PersonalData personalData) {
        Id = id;
        Title = title;
        Status = status;
        PaymentChannel = paymentChannel;
        Description = description;
        AdditionalData = additionalData;
        Amount = amount;
        RegisteredAmount = registeredAmount;
        PersonalData = personalData;
    }

    public string Id{ get; set; }

    public string Title{ get; set; }

    public string Status{ get; set; }

    public string PaymentChannel{ get; set; }

    public string Description{ get; set; }

    public string AdditionalData{ get; set; }

    public Amount Amount{ get; set; }

    public Amount RegisteredAmount{ get; set; }

    public PersonalData PersonalData{ get; set; }
}