﻿using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using OverPay.NET.Apis.CashBillApi.Objects;
using OverPay.NET.Apis.CashBillApi.Responses;
using OverPay.NET.Extensions;
using RestSharp;

namespace OverPay.NET.Apis.CashBillApi; 

public class CashBillApi {
    public string ShopId { get; set; }
    public string SecretPhrase { get; set; }
    public bool Test { get; set; }
    private RestClient _client() => new($"https://pay.cashbill.pl/{(Test ? "test" : "")}ws/rest/");

    public CashBillApi(string shopId, string secretPhrase, bool test) {
        ShopId = shopId;
        SecretPhrase = secretPhrase;
        Test = test;
    }
    
    public List<PaymentChannel> PaymentChannels(){
        var output = new List<PaymentChannel>();
        var result = _client().Get(new RestRequest($"paymentchannels/{ShopId}"));
        try {
            output = JsonConvert.DeserializeObject<List<PaymentChannel>>(result.Content);
        }catch (Exception){}
        return output;
    }
    
    public NewPaymentResponse NewPayment(Order order){
        var output = new NewPaymentResponse();
        try {
            order.Sign = GetOrderSign(order);
            var body = order.ToUrlEncodedString().Result;
            var request = new RestRequest($"payment/{ShopId}")
                .AddParameter("application/x-www-form-urlencoded", body, ParameterType.RequestBody);
            var result = _client().Post(request);
            output = output.FromJson(result.Content);
        }catch (Exception ex){Console.WriteLine(ex);}
        return output;
    }

    public OrderInformation GetOrderInformation(string id){
        var output = new OrderInformation();
        var sign = string.Concat(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(id + SecretPhrase))
            .Select(b => b.ToString("x2")));
        try {
            var result = _client().Get(new RestRequest($"payment/{ShopId}/{id}?sign={sign}"));
            output = JsonConvert.DeserializeObject<OrderInformation>(result.Content);
        }catch (Exception ex){Console.WriteLine(ex);}
        return output;
    }

    public string GetOrderSign(Order order){
        var data = new OrderSignData(order, SecretPhrase);
        var inputData = data.Title +
                    data.Amount.Value +
                    data.Amount.CurrencyCode +
                    data.ReturnUrl +
                    data.Description +
                    data.NegativeReturnUrl +
                    data.AdditionalData +
                    data.PaymentChannel +
                    data.LanguageCode +
                    data.Referer +
                    data.PersonalData?.FirstName +
                    data.PersonalData?.Surname +
                    data.PersonalData?.Email +
                    data.PersonalData?.Country +
                    data.PersonalData?.City +
                    data.PersonalData?.Postcode +
                    data.PersonalData?.Street +
                    data.PersonalData?.House +
                    data.PersonalData?.Flat +
                    data.SecretPhrase;
        return string.Concat(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(inputData))
            .Select(b => b.ToString("x2")));
    }
}