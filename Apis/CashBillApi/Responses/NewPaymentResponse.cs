﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Apis.CashBillApi.Responses; 

public class NewPaymentResponse : Jsonable<NewPaymentResponse> {
    public NewPaymentResponse() {
    }

    public NewPaymentResponse(string id, string redirectUrl) {
        Id = id;
        RedirectUrl = redirectUrl;
    }

    public string Id { get; set; }
    
    public string RedirectUrl { get; set; }
}