﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Apis.CashBillApi.Responses; 

public class TransactionResponse : Jsonable<TransactionResponse> {
    public TransactionResponse() {
    }

    public TransactionResponse(string token, string link) {
        Token = token;
        Link = link;
    }

    public TransactionResponse(NewPaymentResponse newPaymentResponse) {
        Link = newPaymentResponse.RedirectUrl;
        Token = newPaymentResponse.RedirectUrl.Split('/').Reverse().First();
    }

    public string Token { get; set; }
    public string Link { get; set; }
}