﻿using Newtonsoft.Json;

namespace OverPay.NET.Extensions; 

public static class ObjectExtensions {
    public static T To<T>(this object o) => (T) o;
    
    public static async Task<string> ToUrlEncodedString(this object o) {
        var result = await Setup.NodeServices.InvokeAsync<string>("./jsonToUrlEncode", JsonConvert.SerializeObject(o), true);
        return result;
    }
}