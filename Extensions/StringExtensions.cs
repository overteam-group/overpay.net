﻿namespace OverPay.NET.Extensions; 

public static class StringExtensions {
    public static int ToInt(this string s) {
        var output = 0;
        int.TryParse(s, out output);
        return output;
    }
}