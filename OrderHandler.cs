﻿using OverPay.NET.Data;
using OverPay.NET.Extensions;

namespace OverPay.NET; 

public class OrderHandler {
    private User _user { get; set; }
    private Item _item{ get; set; }
    private OverpayContext _context{ get; set; }

    public OrderHandler() {
    }

    public OrderHandler(ref User user, ref Item item, ref OverpayContext overpayContext) {
        _user = user;
        _item = item;
        _context = overpayContext;
    }
    
    public void Handle() {
        switch (_item.Type) {
            default: {
                var user = _context.Users.First(user => user.Id == _user.Id);
                user.Money += _item.AdditionalData!.ToInt();
                _context.SaveChanges();
                break;
            }
        }
    }
}