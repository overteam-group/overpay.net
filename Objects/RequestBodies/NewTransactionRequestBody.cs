﻿namespace OverPay.NET.Objects.RequestBodies;

public class NewTransactionRequestBody {
    public NewTransactionRequestBody() {
    }

    public NewTransactionRequestBody(string nickname, int itemId, string urlReturn, string email, string description) {
        Nickname = nickname;
        ItemId = itemId;
        UrlReturn = urlReturn;
        Email = email;
        Description = description;
    }

    public string Nickname { get; set; }

    public int ItemId{ get; set; }

    public string UrlReturn{ get; set; }

    public string Email{ get; set; }

    public string Description{ get; set; }
}