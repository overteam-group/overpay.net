﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.ConfigObjects; 

public class ApiConfig : XmlSerializable<ApiConfig> {
    public ApiConfig() {
    }

    public ApiConfig(int port, User defaultUser, User admin) {
        Port = port;
        DefaultUser = defaultUser;
        Admin = admin;
    }

    public int Port { get; set; }
    public User DefaultUser { get; set; }
    public User Admin { get; set; }
}