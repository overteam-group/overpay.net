﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.ConfigObjects; 

public class MssqlConfig : XmlSerializable<MssqlConfig> {

    public MssqlConfig() {
    }

    public MssqlConfig(string server, int port, string user, string password, string database) {
        Server = server;
        Port = port;
        User = user;
        Password = password;
        Database = database;
    }

    public string Server { get; set; }
    public int Port { get; set; }
    public string User { get; set; }
    public string Password { get; set; }
    public string Database { get; set; }
}