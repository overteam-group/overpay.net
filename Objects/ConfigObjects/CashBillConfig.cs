﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.ConfigObjects; 

public class CashBillConfig : XmlSerializable<CashBillConfig> {

    public CashBillConfig() {
    }

    public CashBillConfig(string shopId, string secretPhrase, bool test) {
        ShopId = shopId;
        SecretPhrase = secretPhrase;
        Test = test;
    }

    public string ShopId { get; set; }
    public string SecretPhrase { get; set; }
    public bool Test { get; set; }
}