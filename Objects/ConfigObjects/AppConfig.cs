﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.ConfigObjects; 

public class AppConfig : XmlSerializable<AppConfig> {
    public AppConfig() {
    }

    public AppConfig(ApiConfig apiConfig, CashBillConfig cashBillConfig, CashBillOrderConfig cashBillOrderConfig, MssqlConfig mssqlConfig) {
        ApiConfig = apiConfig;
        CashBillConfig = cashBillConfig;
        CashBillOrderConfig = cashBillOrderConfig;
        MssqlConfig = mssqlConfig;
    }

    public ApiConfig ApiConfig { get; set; }
    public CashBillConfig CashBillConfig { get; set; }
    public CashBillOrderConfig CashBillOrderConfig { get; set; }
    public MssqlConfig MssqlConfig { get; set; }
}