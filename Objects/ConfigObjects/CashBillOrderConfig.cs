﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.ConfigObjects; 

public class CashBillOrderConfig : XmlSerializable<CashBillOrderConfig> {
    public CashBillOrderConfig() {
    }

    public CashBillOrderConfig(string defaultReturnUrl) {
        DefaultReturnUrl = defaultReturnUrl;
    }

    public string DefaultReturnUrl { get; set; }
}