﻿using OverPay.NET.Interfaces;

namespace OverPay.NET.Objects.RestedResources; 

public class MojangApiUuid : Jsonable<MojangApiUuid> {
    public MojangApiUuid() {
    }

    public MojangApiUuid(string id, string name) {
        Id = id;
        Name = name;
    }

    public string Id { get; set; }
    public string Name { get; set; }
}