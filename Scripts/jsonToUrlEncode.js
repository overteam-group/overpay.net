﻿module.exports = (callback, json, forceLowerCamelCase) => {
    let output = "";
    json = JSON.parse(json);
    let first = true;
    let prefix = [];

    const camelize = (str) => {
        if(!forceLowerCamelCase) return str;
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
            return index === 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '');
    }

    const extract = (object) => {
        for(let key in object){
            if(typeof(object[key])==='object'){
                prefix.push(camelize(key))
                extract(object[key]);
            }else{
                output += `${first ? '' : '&'}${prefix.length>0 ? `${prefix.join('.')}.` : ""}${camelize(key)}=${encodeURIComponent(object[key])}`;
                first = false;
            }
        }
        prefix.pop()
    };

    extract(json);
    callback(null, output);
};