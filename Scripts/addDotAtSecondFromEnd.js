﻿module.exports = (callback, string) => {
    string = string.toString();
    let output = string.split('').reverse().join('').match(/.{1,2}/g);
    output.splice(1, 0, ".");
    callback(null, output.join("").split('').reverse().join(''));
};