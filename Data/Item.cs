﻿using System;
using System.Collections.Generic;

namespace OverPay.NET.Data
{
    public partial class Item
    {
        public Item()
        {
            Pictures = new HashSet<Picture>();
            Transactions = new HashSet<Transaction>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Description { get; set; } = null!;
        public int Price { get; set; }
        public int CategoryId { get; set; }
        public bool Archived { get; set; }
        public string? Type { get; set; }
        public string? AdditionalData { get; set; }

        public virtual Category Category { get; set; } = null!;
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
