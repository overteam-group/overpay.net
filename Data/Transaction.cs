﻿using System;
using System.Collections.Generic;

namespace OverPay.NET.Data
{
    public partial class Transaction
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int ItemId { get; set; }
        public string OrderId { get; set; } = null!;

        public virtual Item Item { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
