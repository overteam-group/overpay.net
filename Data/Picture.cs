﻿using System;
using System.Collections.Generic;

namespace OverPay.NET.Data
{
    public partial class Picture
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string Name { get; set; } = null!;
        public string Data { get; set; } = null!;

        public virtual Item Item { get; set; } = null!;
    }
}
