﻿using System;
using System.Collections.Generic;

namespace OverPay.NET.Data
{
    public partial class User
    {
        public User()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int Id { get; set; }
        public string Nickname { get; set; } = null!;
        public int Money { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
