﻿using Newtonsoft.Json;

namespace OverPay.NET.Interfaces; 

public abstract class Jsonable<T> {
    public string ToJson() => JsonConvert.SerializeObject(this);
    public T? FromJson(string json) => JsonConvert.DeserializeObject<T>(json);
}