﻿using System.Xml.Serialization;

namespace OverPay.NET.Interfaces; 

public abstract class XmlSerializable<T> {
    public static readonly XmlSerializer XmlSerializer = new(typeof(T));
}