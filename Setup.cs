﻿using System.Xml;
using Microsoft.AspNetCore.NodeServices;
using Newtonsoft.Json;
using OverPay.NET.Apis.CashBillApi;
using OverPay.NET.Extensions;
using OverPay.NET.Objects.ConfigObjects;

namespace OverPay.NET; 

public class Setup {
    private static string _configFileName = "api_config.xml";
    private static XmlWriterSettings _xmlWriterSettings = new XmlWriterSettings() {
        Indent = true, 
        IndentChars = "\t", 
        CloseOutput = true,
        OmitXmlDeclaration = true
    };

    public static INodeServices NodeServices;
    public static CashBillApi CashBillApi;

    public static ApiConfig ApiConfig = new() {
        Port = 50125,
        Admin = new() {
            Login = "admin",
            Password = "admin123"
        },
        DefaultUser = new() {
            Login = "api",
            Password = "api123"
        }
    };

    public static CashBillConfig CashBillConfig = new() {
        ShopId = "some-shop.com",
        SecretPhrase = "my-secret-phrase",
        Test = true
    };

    public static CashBillOrderConfig CashBillOrderConfig = new() {
        DefaultReturnUrl = "https://example.com/afterPayment"
    };
    
    public static MssqlConfig MssqlConfig = new(){
        Server = "localhost",
        Port = 1433,
        User = "sa",
        Password = "password123",
        Database = "overpay"
    };

    public static AppConfig GetAppConfig() => new() {
        ApiConfig = ApiConfig,
        CashBillConfig = CashBillConfig,
        CashBillOrderConfig = CashBillOrderConfig,
        MssqlConfig = MssqlConfig
    };

    public static void SetAppConfig(AppConfig appConfig) {
        ApiConfig = appConfig.ApiConfig;
        CashBillConfig = appConfig.CashBillConfig;
        CashBillOrderConfig = appConfig.CashBillOrderConfig;
        MssqlConfig = appConfig.MssqlConfig;
    }

    public static void OnStart() {
        StartNodeServices();
        if (!File.Exists(_configFileName)) {
            using (var writer = XmlWriter.Create(_configFileName, _xmlWriterSettings)) {
                AppConfig.XmlSerializer.Serialize(writer, GetAppConfig());
                writer.Flush();
            }
        }
        using (var reader = XmlReader.Create(_configFileName)) {
            var appConfig = AppConfig.XmlSerializer.Deserialize(reader)!.To<AppConfig>();
            SetAppConfig(appConfig);
        }
        InitializeCashBillApi();
    }

    public static void StartNodeServices() {
        var services = new ServiceCollection();
        services.AddNodeServices();
        var serviceProvider = services.BuildServiceProvider();
        var options = new NodeServicesOptions(serviceProvider) {
            ProjectPath = "./Scripts"
        };
        NodeServices = NodeServicesFactory.CreateNodeServices(options);
    }

    public static void InitializeCashBillApi() {
        CashBillApi = new CashBillApi(CashBillConfig.ShopId, CashBillConfig.SecretPhrase, CashBillConfig.Test);
    }
}