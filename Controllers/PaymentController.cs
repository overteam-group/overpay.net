﻿using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OverPay.NET.Apis.CashBillApi.Objects;
using OverPay.NET.Apis.CashBillApi.Responses;
using OverPay.NET.Data;
using OverPay.NET.Extensions;
using OverPay.NET.Objects.RequestBodies;

namespace OverPay.NET.Controllers; 

[EnableCors]
[ApiController]
[Route("[controller]/[action]")]
public class PaymentController : ControllerBase{
    [HttpGet]
    [Route("/payment/after")]
    public string After() {
        return "Hello there! (Again)";
    }

    [HttpPost]
    [Route("/payment/newTransaction")]
    public TransactionResponse NewTransaction([FromBody] object body) {
        var output = new TransactionResponse();
        var context = new OverpayContext();
        var request = JsonConvert.DeserializeObject<NewTransactionRequestBody>(body.ToString());
        var item = context.Items.First(searchedItem => searchedItem.Id == request!.ItemId);
        request!.Nickname = Regex.Replace(request!.Nickname, "[^a-zA-Z0-9_]+", "");
        var order = new Order() {
            Title = $"{DateTime.Now.ToFileTime()}-{request.Nickname}-{request.ItemId}",
            Amount = new Amount(
                Setup.NodeServices.InvokeAsync<string>("addDotAtSecondFromEnd", item.Price).Result,
                "PLN"
            ),
            Description = request.Description,
            AdditionalData = "",
            PersonalData = new PersonalData() {
                Email = request.Email
            }
        };
        if (!request.UrlReturn.Equals(string.Empty)) order.ReturnUrl = request.UrlReturn;
        var newPaymentResponse = Setup.CashBillApi.NewPayment(order);
        output = new TransactionResponse(newPaymentResponse);
        return output;
    }

    [HttpGet]
    [Route("/payment/status")]
    public string Status([FromQuery] string cmd, [FromQuery] string args, [FromQuery]string sign) {
        var output = "OK";
        var context = new OverpayContext();
        var argsList = args.Split(",");
        var order = Setup.CashBillApi.GetOrderInformation(argsList[0]);
        if(!order.Status.Equals("PositiveFinish")) return output;

        var parameters = order.Title.Split("-");
        var item = context.Items.First(searchedItem => searchedItem.Id == parameters[2].ToInt());
        var users = context.Users.Where(user => user.Nickname.Equals(parameters[1])).ToList();
        if(users.Count<1) {
            context.Users.Add(new User() {
                Money = 0,
                Nickname = parameters[1]
            });
        }
        users = context.Users.Where(user => user.Nickname.Equals(parameters[1])).ToList();
        var user = users[0];
        var handler = new OrderHandler(ref user, ref item, ref context);
        handler.Handle();
        context.Transactions.Add(new Transaction() {
            ItemId = item.Id,
            OrderId = order.Id,
            Date = DateTime.Now,
            UserId = user.Id
        });
        return output;
    }
}