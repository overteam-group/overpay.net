﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OverPay.NET.Data;

namespace OverPay.NET.Controllers; 

[EnableCors]
[ApiController]
[Route("[controller]/[action]")]
public class CategoryController : ControllerBase {
    [HttpGet]
    [Route("/category/get")]
    public List<Category> Get() => new OverpayContext().Categories.ToList();
}