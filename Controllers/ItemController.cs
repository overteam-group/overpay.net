﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OverPay.NET.Data;

namespace OverPay.NET.Controllers; 

[EnableCors]
[ApiController]
[Route("[controller]/[action]")]
public class ItemController : ControllerBase {
    [HttpGet]
    [Route("/item/get")]
    public List<Item> Get() => new OverpayContext().Items.ToList();

    [HttpGet]
    [Route("/item/getByCategoryId")]
    public List<Item> GetByCategoryId([FromQuery] int categoryId) => new OverpayContext().Items
        .Where(item => item.CategoryId == categoryId)
        .ToList();
    
    [HttpGet]
    [Route("/item/getById")]
    public Item GetById([FromQuery] int id) => new OverpayContext().Items
        .First(item => item.Id == id);
}