﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using OverPay.NET.Data;

namespace OverPay.NET.Controllers; 

[EnableCors]
[ApiController]
[Route("[controller]/[action]")]
public class UserController {
    [HttpGet]
    [Route("/user/get")]
    public List<User> Get() => new OverpayContext().Users.ToList();
    
    [HttpGet]
    [Route("/user/getByNickname")]
    public User GetByNickname([FromQuery] string nickname) => new OverpayContext().Users
        .First(user => user.Nickname.Equals(nickname));

    [HttpGet]
    [Route("/user/getById")]
    public User GetById([FromQuery] int id) => new OverpayContext().Users
        .First(user => user.Id==id);
}