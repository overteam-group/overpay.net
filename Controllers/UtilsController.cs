﻿using System.Buffers.Text;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OverPay.NET.Objects.RestedResources;
using RestSharp;
using RestSharp.Extensions;

namespace OverPay.NET.Controllers; 

[EnableCors]
[ApiController]
[Route("[controller]/[action]")]
public class UtilsController : ControllerBase{
    [HttpGet]
    [Route("/utils/fetchPlayerHead")]
    public IActionResult FetchPlayerHead([FromQuery] string? name, [FromQuery] int? scale) {
        dynamic output = Array.Empty<byte>();
        try {
            var mojangClient = new RestClient("https://api.mojang.com/users/profiles/minecraft/");
            var crafatarClient = new RestClient("https://crafatar.com/renders/head/");
            
            var uuidRequestResponse = mojangClient.Get(new RestRequest(name ?? "Steve"));
            if (uuidRequestResponse.StatusCode != HttpStatusCode.OK) {
                uuidRequestResponse = mojangClient.Get(new RestRequest("Steve"));
            }

            var mojangApiUuid = new MojangApiUuid().FromJson(uuidRequestResponse.Content);
            var headRequestResponse =
                crafatarClient.DownloadData(new RestRequest($"{mojangApiUuid?.Id}?scale={scale ?? 10}&default=MHF_Steve&overlay"));
            output = File(headRequestResponse, "image/jpeg");
        }
        catch (Exception) { }

        return output;
    }
}